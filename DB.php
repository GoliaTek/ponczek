<?php

class DB
{
    private $database;
    private $username;
    private $password;
    private $host;
    private $port;
    public static $connection;

    public function __construct(string $database, string $username, string $password, string $host = 'localhost', string $port = '3307')
    {
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
        $this->host = $host;
        $this->port = $port;
        $this->connection();
    }

    private function connection(bool $force = false): void
    {
        if ($force || !self::$connection) {
            self::$connection = new PDO("mysql:host={$this->host};dbname={$this->database};port={$this->port}", $this->username, $this->password );
        }
    }

    public function query($query)
    {        
        return self::$connection->query($query);
    }
}
?>